package com.drahzek.mvcexercise3.model;

public class UserBuilder {
    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private Integer age;

    public UserBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public UserBuilder setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public UserBuilder setAge(Integer age) {
        this.age = age;
        return this;
    }

    public User createUser() {
        return new User(id, username, firstname, lastname, age);
    }
}