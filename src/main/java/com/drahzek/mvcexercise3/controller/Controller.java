package com.drahzek.mvcexercise3.controller;

import com.drahzek.mvcexercise3.model.User;
import com.drahzek.mvcexercise3.model.UserBuilder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@RestController
public class Controller {

    @RequestMapping(value="/home/{id}", method= RequestMethod.GET)
    public User getUserByID(
            @PathVariable("id") Long id) {
        User user = new UserBuilder().setId(id).createUser();
        return user;
    }

    @RequestMapping(value="/user", method= RequestMethod.POST)
    public String getStringUserByID(
            @RequestBody @Valid User user) {
        System.out.println(user);
        return user.getId().toString();
    }
}
