package com.drahzek.mvcexercise3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcExercise3Application {

	public static void main(String[] args) {
		SpringApplication.run(MvcExercise3Application.class, args);
	}

}

